function xhr() {
	var data = {
		url: null,
		before: null,
		complete: null
	}

	var args = arguments[0] || null;

	if (args) {
		for (var i in args) {
			data[i] = args[i];
		}
	}

	if (data.before) {
		data.before();
	}

	var xhr = new XMLHttpRequest();

	if (data.url) {
		xhr.open('GET', data.url);

		xhr.onload = function() {
			if (xhr.status === 200) {
				var response = JSON.parse(xhr.responseText);

				if (data.complete) {
					data.complete(response);
				}

			} else {
				if (data.error) {
					data.error(xhr.status);
				}
			}
		};

		xhr.send();
	}
}